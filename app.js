const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const authRouter = require('./controllers/routes/auth');
const usersRouter = require('./controllers/routes/users');
const latencyRouter = require('./controllers/routes/latency');

const app = express();


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/auth', authRouter);
app.use('/users', usersRouter);
app.use('/latency', latencyRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://vlad:3RT1W71OYQciSpJA@cluster0-2507l.mongodb.net/test";

MongoClient.connect(uri, function(err, client) {
	authRouter.setDBClient(client);
	usersRouter.setDBClient(client);
	latencyRouter.setDBClient(client);
	// const collection = client.db("test").collection("devices");
	// // perform actions on the collection object
	// client.close();
});

app.use(function (req, res, next) {

	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', '*');

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});

module.exports = app;
