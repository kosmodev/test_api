const crypto = require('crypto');
const Token = function (_id) {
	this.userID = _id;
	this.type = "bearer";
	this.token = crypto.randomBytes(256).toString('hex');
	this.created = new Date().getTime();
};

module.exports = Token;