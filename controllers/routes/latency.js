let express = require('express');
var request = require('request');
let router = express.Router();
let TokenValidator = require('../../utils/tokenValidator');

let mClient;
router.setDBClient = function (client) {
	mClient = client;
};

function elapsedTime(res) {
	request.get({
		url: 'https://www.google.com',
		time: true
	}, function (err, response) {
		res.send({"elapsedTime": response.elapsedTime});
	});
}

router.get('/', function(req, res, next) {
	let token = req.headers.token;
	TokenValidator(mClient
		,res
		,token
		,function (token) {
			elapsedTime(res);
		},function (error, statusCode) {
			res.status(statusCode);
			res.send({"message": error});
		});
});

module.exports = router;
