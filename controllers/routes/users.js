const express = require('express');
let TokenValidator = require('../../utils/tokenValidator');
const router = express.Router();

let mClient;

router.setDBClient = function (client) {
	mClient = client;
};

router.get('/info', function (req, res) {
	let token = req.headers.token;
	TokenValidator(mClient
		,res
		,token
		,function (token) {
			res.send({"id": token.userID});

		},function (error, statusCode) {
			res.status(statusCode);
			res.send({"message": error});
		});
});

module.exports = router;