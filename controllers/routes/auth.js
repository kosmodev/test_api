const express = require('express');

let Token = require('../model/token');
let AuthValidator = require('../../utils/authValidator');
const router = express.Router();

let mClient;
router.setDBClient = function (client) {
	mClient = client;
};

router.post('/signin', function (req, res) {
	let id = req.body["id"];
	let password = req.body["password"];

	let collectionUsers = mClient.db("test").collection("users");
	collectionUsers.findOne({"id": id, "password": password}, function (err, user) {
		if (user) {
			let collectionToken = mClient.db("test").collection("tokens");
			let token = new Token(user.id);
			collectionToken.insert(token);
			res.send(token);
		} else {
			res.status(403);
			res.send({"message": "Not valid id or password"});
		}
	});
});

router.post('/signup', function (req, res) {
	let id = req.body["id"];
	let name = req.body["name"];
	let password = req.body["password"];

	AuthValidator(mClient, id, name, password, res, function (err, status) {
		if (status === 200) {
			let collection = mClient.db("test").collection("users");
			let user = {id, name, password};
			try {
				let isInsert = collection.insert(user);
				console.log(isInsert);
			} catch (e) {
				print("error" + e);
			}
			res.send({"message": "User is created", "user": user});
		} else {
			res.status(status);
			res.send(err);
		}
	});
});

router.get('/logout', function (req,res) {
	let collectionToken = mClient.db("test").collection("tokens");
	let tokenStr = req.headers.token;
	tokenStr = tokenStr.replace("bearer ","");
	let isAll = req.query.all;
	if (isAll) {
		collectionToken.findOne({"token": tokenStr}, function (err, token) {
			if (token) {
				collectionToken.deleteMany({"userID": token.userID}, function (err, obj) {
					if (err) throw err;
					res.send({"message": "Logout done"});
				});
			} else {
				res.send({"message": "Not found token"});
			}
		});
	} else {
		collectionToken.deleteOne({"token": tokenStr}, function (err, obj) {
			if (err) throw err;
			res.send({"message": "Logout done"});
		});
	}
});

module.exports = router;