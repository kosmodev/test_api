module.exports = function(mClient, res, token, callbackDone, callbackError){
	let collectionToken = mClient.db("test").collection("tokens");

	if (!token) {
		res.status(403);
		res.send({"message": "Access denied authorization failed"});
		return;
	}

	token = token.replace("bearer ","");

	collectionToken.findOne({"token": token}, function (err, token) {
		if (token) {
			let currentTimestamp = new Date().getTime();
			let minutes = ((currentTimestamp - token.created)/1000)/60;
			if (minutes < 10) {
				callbackDone(token);
			} else {
				callbackError("Old token.",404);
			}
		} else {
			res.status(404);
			res.send({"message": "Access denied authorization failed not found token"});
		}
	});
};