const validator = require("email-validator");
function isValid(p) {
	const phoneRe = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
	const digits = p.replace(/\D/g, "");
	return phoneRe.test(digits);
}

module.exports = function isValidSignupData(mClient,id,name,password,res,callbakc){
	let collection = mClient.db("test").collection("users");

	collection.findOne({id:id}, function (err, user) {
		if (user) {
			callbakc({"message":"A user with this id exists"},400);
			return;
		}

		if (!id) {
			callbakc({"message":"Error id is empty"},400,null);
			return;
		} else if (!isValid(id) && !validator.validate(id)) {
			callbakc({"message":"Error id or email not valid"},400);
			return;
		}

		if (!name) {
			callbakc({"message":"Error name is empty"},400,null);
			return;
		}

		if (!password) {
			callbakc({"message":"Error password is empty"},400);
			return;
		}

		callbakc(null,200);
	});
};


